/*
 * Name:    trgEmployeeUpdateBefore
 * Version: 2.2
 * Description: 
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */
trigger trgEmployeeUpdateBefore on T1C_Base__Employee__c (after update, before delete) 
{
	//check if the trigger is active
    //if it is not, abort
    if(T1C_Base.ACETriggerCheck.isDisabled('trgEmployeeUpdateBefore'))
    {
    	return;
    }
    
	AceAccountCoverageRollDown acRollDown = new AceAccountCoverageRollDown();
	if(Trigger.isUpdate)
	{
		acRollDown.determineUpdateContactCoverageFromEmployees(Trigger.old, Trigger.new);
	}
	if(Trigger.isDelete)
	{
		acRollDown.deleteContactCoverageFromEmployees(Trigger.old);
	}
}