/*
 * Name:    trgAccountCoverageCreateUpdateDeleteBefore
 * Version: 2.2
 * Description: 
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */
trigger trgAccountCoverageCreateUpdateDeleteBefore on T1C_Base__Account_Coverage__c (before delete, before insert, before update)
{
	//check if the trigger is active
    //if it is not, abort
    if(T1C_Base.ACETriggerCheck.isDisabled('trgAccountCoverageCreateUpdateDeleteBefore')  || 
		!AceAccountCoverageRollDown.finishedTriggers.add('trgAccountCoverageCreateUpdateDeleteBefore'))
    {
    	return;
    }
	
	//We shouldnt be calling the Contact Coverage trigger after this one inserts, updates, or deletes Contact Coverage records
	AceAccountCoverageRollDown.finishedTriggers.add('contCoverageRollupAfter');

	AceAccountCoverageRollDown acRollDown = new AceAccountCoverageRollDown();
	if(Trigger.isInsert)
	{
		acRollDown.insertContactCoverageFromAccountCoverages(Trigger.new);
	}
	else if(Trigger.isUpdate)
	{
		acRollDown.determineUpdateContactCoverageFromAccountCoverages(Trigger.old, Trigger.new);
	}
	else if(Trigger.isDelete)
	{
		acRollDown.deleteContactCoverageFromAccountCoverages(Trigger.old);
	}
}