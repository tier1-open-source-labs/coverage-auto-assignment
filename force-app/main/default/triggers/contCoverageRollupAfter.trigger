/** 
 * Name: contCoverageRollupAfter 
 * Version: 2.2
 * Description: Trigger to do the Contact coverage rollup
 *
 * Confidential & Proprietary, 2013 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
 
trigger contCoverageRollupAfter on T1C_Base__Contact_Coverage__c (after insert, after update, before delete) 
{
    //check if the trigger is active
    //if it is not, abort
    if( T1C_Base.ACETriggerCheck.isDisabled('contCoverageRollupAfter') || 
        !AceAccountCoverageRollDown.finishedTriggers.add('contCoverageRollupAfter') )
    {
    	return;
    }	

    //We shouldnt be calling the Account Coverage trigger after this one inserts, updates, or deletes Account Coverage records
    AceAccountCoverageRollDown.finishedTriggers.add('trgAccountCoverageCreateUpdateDeleteBefore');

    String PRIMARY = 'PRIMARY';
    String BACKUP = 'BACK';

//  Filter the Contact Coverage records.
//  ------------------------------------
    Set<Id> trgCCIdSet = ( Trigger.isDelete ? Trigger.oldMap : Trigger.newMap ).keySet ();
    Set<Id> filterCCIdSet = filterCCRecs ( trgCCIdSet );
    for ( Id filterCCId : filterCCIdSet ) 
    {
        System.debug ( LoggingLevel.INFO , 'CC Rec filtered by Cont_Cov_Rollup_Filter - NOT ROLLING UP:\n'
                                            + ( Trigger.isDelete ? Trigger.oldMap : Trigger.newMap ).get ( filterCCId ) );
    }

//  Filter out TEMPORARY CC records as well.
//  ----------------------------------------
    for ( T1C_Base__Contact_Coverage__c cc : Trigger.isDelete ? Trigger.old : Trigger.new ) 
    {
        if ( !filterCCIdSet.contains ( cc.Id ) ) 
        {
            if ( cc.T1C_Base__Is_Temporary__c == 'true' ) 
            {
                filterCCIdSet.add ( cc.Id );
            }
        }
    }
    
    if ( filterCCIdSet.size () == trgCCIdSet.size () ) 
    {
        
//      All Contact Coverage records matched the filter - nothing to do!
//      ----------------------------------------------------------------
        return;
    }

//  Grab the Contacts associated with our Contact Coverages.  Also, populate the
//  EmplId -> ContId[] map with any newly-covered Contacts (i.e. any inserted Coverages
//  or the new Contact on any Coverage whose Contact has changed).
//  -----------------------------------------------------------------------------------     
    Set<Id> allContIdSet = new Set<Id> {};
    Map<Id , Set<Id>> newEmplContIdMap = new Map<Id , Set<Id>> {};
    
    for ( T1C_Base__Contact_Coverage__c cc : ( Trigger.isDelete ? Trigger.old : Trigger.new ) ) 
    {
        
        if ( filterCCIdSet.contains ( cc.Id ) ) 
        {   
            continue;   // --- CC rec has been filtered out ---
        }

        System.debug ( LoggingLevel.INFO , 'ROLLING UP CONT COV :\n' + cc );
                
        if ( Trigger.isUpdate ) 
        {
            T1C_Base__Contact_Coverage__c oldCC = Trigger.oldmap.get ( cc.Id );
            if ( oldCC.T1C_Base__Role__c != cc.T1C_Base__Role__c 
                    || oldCC.T1C_Base__Start_Date__c != cc.T1C_Base__Start_Date__c
                    || oldCC.T1C_Base__End_Date__c != cc.T1C_Base__End_Date__c ) 
            {
                allContIdSet.add ( cc.T1C_Base__Contact__c );
            }
            if ( oldCC.T1C_Base__Contact__c != cc.T1C_Base__Contact__c ) 
            {
                allContIdSet.add ( cc.T1C_Base__Contact__c );
                allContIdSet.add ( oldCC.T1C_Base__Contact__c );
                if ( !newEmplContIdMap.containsKey ( cc.T1C_Base__Employee__c ) ) 
                {
                    newEmplContIdMap.put ( cc.T1C_Base__Employee__c , new Set<Id> {} );
                }
                newEmplContIdMap.get ( cc.T1C_Base__Employee__c ).add ( cc.T1C_Base__Contact__c );
            }
        }
        else if ( Trigger.isInsert ) 
        {
            allContIdSet.add ( cc.T1C_Base__Contact__c );
            
            if ( !newEmplContIdMap.containsKey ( cc.T1C_Base__Employee__c ) ) 
            {
                newEmplContIdMap.put ( cc.T1C_Base__Employee__c , new Set<Id> {} );
            }
            newEmplContIdMap.get ( cc.T1C_Base__Employee__c ).add ( cc.T1C_Base__Contact__c );
        }
        else if ( Trigger.isDelete ) 
        {
            allContIdSet.add ( cc.T1C_Base__Contact__c );
        }
    }

//  Grab the Accounts associated with our Coverages' Contacts.
//  ----------------------------------------------------------
    Map<Id , Id> acctIdByContId = new Map<Id , Id> {};
    Map<Id , Set<Id>> newEmplAcctIdMap = new Map<Id , Set<Id>> {};

    for ( Contact cont : [ select
                                AccountId
                            from
                                Contact
                            where
                                Id in :allContIdSet
                        ]
    ) 
    {
        acctIdByContId.put ( cont.Id , cont.AccountId );
        for ( Id emplId : newEmplContIdMap.keySet () ) 
        {
            if ( newEmplContIdMap.get ( emplId ).contains ( cont.Id ) ) 
            {
                if ( !newEmplAcctIdMap.containsKey ( emplId ) ) 
                {
                    newEmplAcctIdMap.put ( emplId , new Set<Id> {} );
                }
                newEmplAcctIdMap.get ( emplId ).add ( cont.AccountId );
            }
        }
    }
    System.debug ( LoggingLevel.INFO , 'CONTID->ACCTID MAP [' + acctIdByContId + ']' );

//  We have created newEmplAcctIdMap containing every AccountId associated with a newly-covered Contact
//  in our trigger CCs.  In Insert context, this would be: all CCs, and in Update context, this would
//  be all CCs whose Contact has changed - add the AccountId associated with the new Contact.  But
//  this over-counts the Accounts, as there may be other Contacts in our Update trigger which are
//  NOT 'new' and are associated with the same Accounts.  Filter these cases out now.
//  ---------------------------------------------------------------------------------------------------
    if ( Trigger.isUpdate ) 
    {
        for ( T1C_Base__Contact_Coverage__c occ : Trigger.old ) 
        {
            if ( filterCCIdSet.contains ( occ.Id ) ) 
            {  
                continue;   // --- CC rec has been filtered out ---
            }
            
            Id acctId = acctIdByContId.get ( occ.T1C_Base__Contact__c );
            if ( newEmplAcctIdMap.containsKey ( occ.T1C_Base__Employee__c ) ) 
            {
                newEmplAcctIdMap.get ( occ.T1C_Base__Employee__c ).remove ( acctId );
            }
        }
    }
    
//  Grab the existing Account Coverages linking these Accounts with our Contact Coverages' Employees.
//  -------------------------------------------------------------------------------------------------
	Map<Id , Set<Id>> emplIdSetByAcct = new Map<Id , Set<Id>> {};

    T1C_Base__Account_Coverage__c[] acList = new T1C_Base__Account_Coverage__c[] {};
    T1C_Base__Contact_Coverage__c[] newCCList = new T1C_Base__Contact_Coverage__c[] {};
    
    for ( T1C_Base__Contact_Coverage__c cc : ( Trigger.isDelete ? Trigger.old : Trigger.new ) ) 
    {

        if ( filterCCIdSet.contains ( cc.Id ) ) 
        {   
            continue;   // --- CC rec has been filtered out ---
        }
        
        Id acctId = acctIdByContId.get ( cc.T1C_Base__Contact__c );
        if ( acctId != Null ) 
        {
			if ( !emplIdSetByAcct.containsKey ( acctId ) ) 
			{
				emplIdSetByAcct.put ( acctId , new Set<Id> {} );
			}
			emplIdSetByAcct.get ( acctId ).add ( cc.T1C_Base__Employee__c );

            if ( Trigger.isInsert ) 
            {
                newCCList.add ( cc );
            }
        }
        if ( Trigger.isUpdate ) 
        {
            T1C_Base__Contact_Coverage__c occ = Trigger.oldMap.get ( cc.Id );
            if ( occ.T1C_Base__Contact__c != cc.T1C_Base__Contact__c ) 
            {
                Id oldAcctId = acctIdByContId.get ( occ.T1C_Base__Contact__c );
                if ( oldAcctId != Null ) 
                {
					if ( !emplIdSetByAcct.containsKey ( oldAcctId ) ) 
					{
						emplIdSetByAcct.put ( oldAcctId , new Set<Id> {} );
					}
					emplIdSetByAcct.get ( oldAcctId ).add ( occ.T1C_Base__Employee__c );

                    newCCList.add ( cc );
                }
            }
        }
    }

//	Build Account Coverage and Contact Coverage query clauses out of the AcctId -> EmplIdSet map.
//	---------------------------------------------------------------------------------------------
	String acQryClause , ccQryClause;
	if ( !emplIdSetByAcct.isEmpty () ) 
	{
		String[] acQryFragList = new String[] {};
		String[] ccQryFragList = new String[] {};

		for ( Id acctId : emplIdSetByAcct.keySet () ) 
		{
			Set<Id> emplIdSet = emplIdSetByAcct.get ( acctId );
			String emplFrag = 'T1C_Base__Employee__c in ( ' + T1C_Base.AceUtil.concatList ( new List<Id> ( emplIdSet ) , ' , ' , True ) + ' )';
			acQryFragList.add ( '( Account__c = \'' + acctId + '\' and ' + emplFrag + ' )' );
			ccQryFragList.add ( '( T1C_Base__Contact__r.AccountId = \'' + acctId + '\' and ' + emplFrag + ' )' );
		}
		
		acQryClause = '( ' + T1C_Base.AceUtil.concatList ( acQryFragList , ' ) or ( ' ) + ' )';
		ccQryClause = '( ' + T1C_Base.AceUtil.concatList ( ccQryFragList , ' ) or ( ' ) + ' )';
	}
	
    System.debug ( LoggingLevel.INFO , 'AC QRY CLAUSE [' + acQryClause + ']' );
    System.debug ( LoggingLevel.INFO , 'CC QRY CLAUSE [' + ccQryClause + ']' );
    System.debug ( LoggingLevel.INFO , 'NEW CC LIST [' + newCCList + ']' );
    
//  Do the Account Coverage query (if there is any to do) and organize the results
//  by Employee and Account.
//  ------------------------------------------------------------------------------
	if ( acQryClause != Null ) 
	{
        acList = (List<T1C_Base__Account_Coverage__c>) (
                        T1C_Base.AceSObject.getSObjectInfo ( 'T1C_Base__Account_Coverage__c' , acQryClause , Null , Null , 
                                                    new String[] { 'T1C_Base__Start_Date__c' , 'T1C_Base__End_Date__c' , 'T1C_Base__Role__c' , 'T1C_Base__Account__c' , 'T1C_Base__Employee__c' } )
                    );
	}

    Map<Id , Map<Id , T1C_Base__Account_Coverage__c[]>> ACsByEmplAcct = new Map<Id , Map<Id , T1C_Base__Account_Coverage__c[]>> {};
    for ( T1C_Base__Account_Coverage__c ac : acList ) 
    {
        if ( !ACsByEmplAcct.containsKey ( ac.T1C_Base__Employee__c ) ) 
        {
            ACsByEmplAcct.put ( ac.T1C_Base__Employee__c , new Map<Id , T1C_Base__Account_Coverage__c[]> {} );
        }
        if ( !ACsByEmplAcct.get ( ac.T1C_Base__Employee__c ).containsKey ( ac.T1C_Base__Account__c ) ) 
        {
            ACsByEmplAcct.get ( ac.T1C_Base__Employee__c ).put ( ac.T1C_Base__Account__c , new T1C_Base__Account_Coverage__c[] {} );
        }
        ACsByEmplAcct.get ( ac.T1C_Base__Employee__c ).get ( ac.T1C_Base__Account__c ).add ( ac );
    }
    
//  Construct the 'ideal' Account Coverages based on what's in the database (update/delete only).
//  ---------------------------------------------------------------------------------------------
    Map<Id , Map<Id , T1C_Base__Account_Coverage__c>> idealACByEmplAcct = new Map<Id , Map<Id , T1C_Base__Account_Coverage__c>> {};
    
    if ( Trigger.isUpdate || Trigger.isDelete ) 
    {
        T1C_Base__Contact_Coverage__c[] ccList = new T1C_Base__Contact_Coverage__c[] {};

		if ( ccQryClause != Null ) 
		{
            ccList = (List<T1C_Base__Contact_Coverage__c>) (
                            T1C_Base.AceSObject.getSObjectInfo ( 'T1C_Base__Contact_Coverage__c' , ccQryClause , Null , Null , 
                                                        new String[] { 'T1C_Base__Start_Date__c' , 'T1C_Base__End_Date__c' , 'T1C_Base__Role__c' , 'T1C_Base__Contact__r.AccountId' , 'T1C_Base__Employee__c' } )
                        );
        }

        for ( T1C_Base__Contact_Coverage__c cc : ccList ) 
        {
            
//          Omit CCs from DB that are part of this trigger.  Required since this trigger was
//          moved from 'after delete' to 'before delete' to make it play nicer with other
//          Coverage-related triggers.
//          --------------------------------------------------------------------------------
            if ( Trigger.isDelete && Trigger.oldMap.containsKey ( cc.Id ) ) 
            {
                continue;
            }
            applyCCToACMap ( cc , cc.T1C_Base__Contact__r.AccountId , idealACByEmplAcct );
        }
    }
    
//  Extend the 'ideal' Account Coverages based on the records we're inserting, plus the new Contacts
//  being covered as a result of Contact-changing updates.
//  ------------------------------------------------------------------------------------------------
    for ( T1C_Base__Contact_Coverage__c cc : newCCList ) 
    {
        Id acctId = acctIdByContId.get ( cc.T1C_Base__Contact__c );
        applyCCToACMap ( cc , acctId , idealACByEmplAcct );
    }

    System.debug ( LoggingLevel.INFO , 'ACS BY EMPL ACCT:' );
    for ( Id emplId : ACsByEmplAcct.keySet () ) 
    {
        for ( Id acctId : ACsByEmplAcct.get ( emplId ).keySet () ) 
        {
            System.debug ( LoggingLevel.INFO , 'EMPL [' + emplId + '] ACCT [' + acctId + '] [' + ACsByEmplAcct.get ( emplId ).get ( acctId ) + ']' );           
        }
    }
    System.debug ( LoggingLevel.INFO , 'IDEAL ACS BY EMPL ACCT:' );
    for ( Id emplId : idealACByEmplAcct.keySet () ) 
    {
        for ( Id acctId : idealACByEmplAcct.get ( emplId ).keySet () ) 
        {
            System.debug ( LoggingLevel.INFO , 'EMPL [' + emplId + '] ACCT [' + acctId + '] [' + idealACByEmplAcct.get ( emplId ).get ( acctId ) + ']' );           
        }
    }

//  Now merge the two, looking for Account Coverages to be inserted, updated and removed.
//  -------------------------------------------------------------------------------------
    T1C_Base__Account_Coverage__c[] insACList = new T1C_Base__Account_Coverage__c[] {};
    T1C_Base__Account_Coverage__c[] updACList = new T1C_Base__Account_Coverage__c[] {};
    T1C_Base__Account_Coverage__c[] delACList = new T1C_Base__Account_Coverage__c[] {};
    
    for ( Id emplId : ACsByEmplAcct.keySet () ) 
    {
        for ( Id acctId : ACsByEmplAcct.get ( emplId ).keySet () ) 
        {
            if ( !idealACByEmplAcct.containsKey ( emplId )
                    || !idealACByEmplAcct.get ( emplId ).containsKey ( acctId ) ) 
            {
                        
//              No 'ideal' Acct Cov matching this 'actual' Acct Cov --> delete it.
//              ------------------------------------------------------------------                      
                delACList.addAll ( ACsByEmplAcct.get ( emplId ).get ( acctId ) );
            }
            else 
            {
                T1C_Base__Account_Coverage__c idealAC = idealACByEmplAcct.get ( emplId ).get ( acctId );
                for ( T1C_Base__Account_Coverage__c ac : ACsByEmplAcct.get ( emplId ).get ( acctId ) ) 
                {
                    if ( newEmplAcctIdMap.containsKey ( emplId ) && newEmplAcctIdMap.get ( emplId ).contains ( acctId ) ) 
                    {
                        
//                      NEW 'ideal' Acct Cov --> merge with 'actual' Acct Cov and update if necessary.
//                      ------------------------------------------------------------------------------
                        if ( mergeAcctCoverage ( ac , idealAC ) ) 
                        {
                            updACList.add ( ac );
                        }
                    }
                    else 
                    {
                        
//                      'Ideal' Acct Cov built from DB --> apply to 'actual' Acct Cov and update.
//                      -------------------------------------------------------------------------
                        ac.T1C_Base__Start_Date__c = idealAC.T1C_Base__Start_Date__c;
                        ac.T1C_Base__End_Date__c = idealAC.T1C_Base__End_Date__c;
                        ac.T1C_Base__Role__c = idealAC.T1C_Base__Role__c;
                        updACList.add ( ac );
                    }
                }
                idealACByEmplAcct.get ( emplId ).remove ( acctId );
            }
        }
    }
    
//  The remaining 'ideal' Account Coverages are to be inserted.
//  -----------------------------------------------------------
    for ( Id emplId : idealACByEmplAcct.keySet () ) 
    {
        insACList.addAll ( idealACByEmplAcct.get( emplId ).values () );
    }
    
    System.debug ( LoggingLevel.INFO , 'INSERT ACS :' );
    for ( T1C_Base__Account_Coverage__c ac : insACList ) 
    {
        System.debug ( LoggingLevel.INFO , ac );
    }   
    System.debug ( LoggingLevel.INFO , 'UPDATE ACS :' );
    for ( T1C_Base__Account_Coverage__c ac : updACList ) 
    {
        System.debug ( LoggingLevel.INFO , ac );
    }   
    System.debug ( LoggingLevel.INFO , 'DELETE ACS :' );
    for ( T1C_Base__Account_Coverage__c ac : delACList ) 
    {
        System.debug ( LoggingLevel.INFO , ac );
    }
    
    insert insACList;
    update updACList;
    delete delACList;
    
    
//  ============================================================================
//  Method: filterCCRecs
//
//  Desc:   Reaches into the 'Cont_Cov_Rollup_Filter__c' custom setting to get
//          org-specific filters on the Contact Coverage object.  Runs the 
//          specified set of Contact Coverage Ids through these filters; 
//          any record matching any one of the filters (i.e. the filters are 
//          combined with 'OR') is included in the returned Id set.
//
//  Args:   ccIdSet - the set of Contact Coverage Ids to filter.
//
//  Return: A subset of the Contact Coverage Id set, identifying the Contact
//          Coverage records matching any of the filters from the custom
//          setting.
//          * If the custom setting is absent or empty, a blank set is returned.
//          * If the custom setting contains invalid data so that the resulting
//            SOQL query is invalid, an error is thrown.
//  ============================================================================                        
    Set<Id> filterCCRecs ( Set<Id> ccIdSet ) 
    {
        Set<Id> filteredCCIdSet = new Set<Id> {};
         
//      Grab the filters from the Custom Setting.
//      -----------------------------------------
        SObject[] ccrfList;
        String soqlQry = 'select '
                            + 'Name , '
                            + 'Exclusion_Filter__c, '
                            + 'FieldName__c '
                        + 'from '
                            + 'Cont_Cov_Rollup_Filter__c';
        try 
        {
            ccrfList = Database.query ( soqlQry );
        }
        catch ( QueryException e ) 
        {
            
//          Custom setting not defined.  Short-circuit without doing any filtering.
//          -----------------------------------------------------------------------
            return filteredCCIdSet;
        }
        if ( ccrfList.isEmpty () ) 
        {
            return filteredCCIdSet;
        }

//      Now construct the filter query based on the retrieved Custom Settings.
//      ----------------------------------------------------------------------
        soqlQry = 'select '
                        + 'Id '
                    + 'from '
                        + 'T1C_Base__Contact_Coverage__c '
                    + 'where '
                        + 'Id in ( ' + T1C_Base.AceUtil.concatList ( new List<Id> ( ccIdSet ) , ' , ' , True ) + ' ) ';
        
        String[] whereClauseList = new String[] {};
        for ( SObject ccrf : ccrfList ) 
        {
             whereClauseList.add ( ccrf.get ( 'FieldName__c' ) + ' ' + ccrf.get ( 'Exclusion_Filter__c' ) );
        }
        soqlQry += 'and ( '
                        + T1C_Base.AceUtil.concatList ( whereClauseList , ' or ' )
                    + ' )';
                    
        T1C_Base__Contact_Coverage__c[] ccList;
        try 
        {
            System.debug ( LoggingLevel.INFO , 'CC Filter Qry : [' + soqlQry + ']' );
            ccList = (List<T1C_Base__Contact_Coverage__c>)( Database.query ( soqlQry ) );
        }
        catch ( QueryException e ) 
        {
            System.assert ( False , 'Invalid Contact Coverage Rollup Filter Custom setting' );
        }
        
        for ( T1C_Base__Contact_Coverage__c cc : ccList ) 
        {
            filteredCCIdSet.add ( cc.Id );
        }
        
        return filteredCCIdSet;
    }
    
//  ============================================================================
//  Method: applyCCToACMap
//
//  Desc:   Populates the specified EmplId->AcctId->AccountCoverage map with
//          the information from the specified ContactCoverage record and 
//          Account Id.
//
//  Args:   cc              - the Contact Coverage.
//          acctId          - the Account Id (typically associated with the CC's Contact)
//          acByEmplAcct    - The AccountCoverage map
//
//  Return: Nothing.  The AccountCoverage map is modified in situ.
//  ============================================================================                        
    void applyCCToACMap ( T1C_Base__Contact_Coverage__c cc , Id acctId , Map<Id , Map<Id , T1C_Base__Account_Coverage__c>> acByEmplAcct ) 
    {
        System.debug ( LoggingLevel.INFO , 'Merging CC into AC Map :\n' + cc );
        
        if ( acctId == Null ) 
        {
            return;
        }
        if ( !idealACByEmplAcct.containsKey ( cc.T1C_Base__Employee__c ) ) 
        {
            acByEmplAcct.put ( cc.T1C_Base__Employee__c , new Map<Id , T1C_Base__Account_Coverage__c> {} );
        }
        T1C_Base__Account_Coverage__c ac = acByEmplAcct.get ( cc.T1C_Base__Employee__c ).get ( acctId );
        T1C_Base__Account_Coverage__c newAC = new T1C_Base__Account_Coverage__c ( T1C_Base__Account__c = acctId , T1C_Base__Employee__c = cc.T1C_Base__Employee__c ,
                                                                T1C_Base__Start_Date__c = cc.T1C_Base__Start_Date__c , T1C_Base__End_Date__c = cc.T1C_Base__End_Date__c ,
                                                                T1C_Base__Role__c = cc.T1C_Base__Role__c );
        if ( ac == Null ) 
        {
            acByEmplAcct.get ( cc.T1C_Base__Employee__c ).put ( acctId , newAC );
        }
        else 
        {
            mergeAcctCoverage ( ac , newAC );
        }
    }
    
//  ============================================================================
//  Method: mergeAcctCoverage
//
//  Desc:   Merges the specified 'source' Account Coverage with the specified
//          'target' Account Coverage:
//          * Uses the earlier Start Date and later End Date of the two.
//          * Uses the 'better' Role of the two (see CompareRoles()).
//
//  Args:   tgtAC   - the 'target' Account Coverage
//          srcAC   - the 'source' Account Coverage
//
//  Return: True if the target Account Coverage was modified as a result of
//          the merge.
//  ============================================================================                        
    Boolean mergeAcctCoverage ( T1C_Base__Account_Coverage__c tgtAC , T1C_Base__Account_Coverage__c srcAC ) 
    {
        Boolean isTgtChanged = False;
        
        System.debug ( LoggingLevel.INFO , 'Merging SRC AC :\n' + srcAC + '\ninto TGT AC :\n' + tgtAC );
        if ( tgtAC.T1C_Base__Start_Date__c != Null && ( srcAC.T1C_Base__Start_Date__c == Null || srcAC.T1C_Base__Start_Date__c < tgtAC.T1C_Base__Start_Date__c ) ) 
        {
            tgtAC.T1C_Base__Start_Date__c = srcAC.T1C_Base__Start_Date__c;
            isTgtChanged = True;
        }
        if ( tgtAC.T1C_Base__End_Date__c != Null && ( srcAC.T1C_Base__End_Date__c == Null || srcAC.T1C_Base__End_Date__c > tgtAC.T1C_Base__End_Date__c ) ) 
        {
            tgtAC.T1C_Base__End_Date__c = srcAC.T1C_Base__End_Date__c;
            isTgtChanged = True;
        }
        if ( compareRoles ( tgtAC.T1C_Base__Role__c , srcAC.T1C_Base__Role__c ) ) 
        {
            tgtAC.T1C_Base__Role__c = srcAC.T1C_Base__Role__c;
            isTgtChanged = True;
        }
        
        System.debug ( LoggingLevel.INFO , '...IS CHANGED [' + isTgtChanged + '] :\n' + tgtAC );
        return isTgtChanged;
    }
                
//  ============================================================================
//  Method: compareRoles
//
//  Desc:   Compares the specified 'target' and 'source' Coverage Role.  Roles,
//          in 'better-to-worse' order, are:
//          * Anything containing 'Primary'.
//          * Anything containing 'Back'.
//          * Any non-Null value.
//          * Null.
//
//  Args:   tgtRole - the 'target' Role
//          srcRole - the 'source' Role
//
//  Return: True if the source Role is 'better' than the target Role; False
//          otherwise.
//  ============================================================================                        
    Boolean compareRoles ( String tgtRole , String srcRole ) 
    {
        if ( srcRole == Null ) 
        {
            return False;
        }
        srcRole = srcRole.toUpperCase ();
        
        if ( tgtRole == Null ) 
        {
            return True;
        }
        tgtRole = tgtRole.toUpperCase ();
        
        if ( tgtRole.contains ( PRIMARY ) ) 
        {
            return False;
        }
        else if ( tgtRole.contains ( BACKUP ) ) 
        {
            return ( srcRole.contains ( PRIMARY ) );
        }
        else 
        {
            return ( srcRole.contains ( PRIMARY ) || srcRole.contains ( BACKUP ) );
        }
    }
}