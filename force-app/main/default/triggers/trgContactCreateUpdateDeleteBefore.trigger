/*
 * Name:    trgContactCreateUpdateDeleteBefore
 * Version: 2.2
 * Description: 
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */
trigger trgContactCreateUpdateDeleteBefore on Contact (after insert, after update, before delete) 
{
	//check if the trigger is active
    //if it is not, abort
    if(T1C_Base.ACETriggerCheck.isDisabled('trgContactCreateUpdateDeleteBefore'))
    {
    	return;
    }
    
	AceAccountCoverageRollDown acRollDown = new AceAccountCoverageRollDown();
	if(Trigger.isInsert)
	{
		acRollDown.insertContactCoverageFromContacts(Trigger.new);
	}
	if(Trigger.isDelete)
	{
		acRollDown.deleteContactCoverageFromContacts(Trigger.old);
	}
	if(Trigger.isUpdate)
	{
		acRollDown.determineUpdateContactCoverageFromContact(Trigger.old, Trigger.new);
	}
}