/*
 * Name:    AceAccountCoverageRollDown
 * Version: 2.2

 * Description: 
 * Helper class for trgAccountCoverageCreateUpdateDeleteBeore.trigger, trgContactCreateUpdateDeleteBefore.trig, trgEmployeeUpdateBefore.trigger
 * Synchronize Contact Coverage with Account Coverage.  This will fire when an Account Coverage is create/updated/deleted, but also when
 * a Contact is created/updated/deleted and an Employee updated or deleted.
 *
 * The public insert/delete methods on the class go through the following logic:
 * 1) Start with either account coverages, contacts or employees
 * 2) Get the account coverages
 * 3) Get the employees associated with those account coverages
 * 4) Filter the employees
 * 5) Filter the account coverage based on the filter employees
 * 6) Get contacts, filtered on the account coverage/contact filter
 * 7) Create/delete the required contact coverages
 *
 * The public update methods go through the following logic:
 
 * If T1C_Base__Account_Coverage__c field changes:
 * T1C_Base__Employee__c: cannot change
 * Account__c: delete old and create new
 * Product__c: delete old and create new
 * All other fields: update CC
 *
 * If Contact field changes:
 * Decision_Maker_For__c: delete, maintain existing, add new
 *
 * If Employee field changes:
 * Department__c: delete old, and create new 
 *
 * @author ahwang 
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */
public class AceAccountCoverageRollDown
{
	///////////////////////////////////////////////////////////////////////////
	// CONSTANTS	
	///////////////////////////////////////////////////////////////////////////
	static final String ACCOUNT_FIELD = 'T1C_Base__Account__c';
	static final String ACCOUNT_ID_FIELD = 'AccountId';
	static final String CONTACT_FIELD = 'T1C_Base__Contact__c';
	static final String CONTACT_RELATIONSHIP = 'T1C_Base__Contact__r';
	
	// batch size for insert/upsert/update/delete
	static final integer MAX_BATCH_SIZE = 200;
	
	
	///////////////////////////////////////////////////////////////////////////
	// Static variables
	///////////////////////////////////////////////////////////////////////////
	
	// map from Account Coverage field to Contact Coverage field
	static Map<String,String> accountCoverageFieldByContactCoverageFieldMap = new Map<String,String>{};

	// map from Employee field to value
	static Map<String, Set<String>> employeeFilterMap = new Map<String, Set<String>>{};
	
	// map from Account Coverage field to Contact field
	// Note: only one map is currently supported
	static Map<String, String> contactToAccountCoverageFilterMap = new Map<String, String> {};

	// Contact field for map from Account Coverage field to Contact field
	static String contactToAccountCoverageField;
	
	// Account Coverage field for map from Account Coverage field to Contact field
	static String accountCoverageToContactField;
	
	public static Set<String> finishedTriggers = new Set<String>();
	///////////////////////////////////////////////////////////////////////////
	// Instance variables
	///////////////////////////////////////////////////////////////////////////
	T1C_Base__Account_Coverage__c[] accountCoverages;
	Contact[] contacts = new Contact[]{};
	Set<Id> employeeIds;
	boolean isConfigured = false;
	
	
	///////////////////////////////////////////////////////////////////////////
	// Constructor
	///////////////////////////////////////////////////////////////////////////
	public AceAccountCoverageRollDown()
	{
		for(ContactCoverageAccountCoverageFieldPairs__c pair : ContactCoverageAccountCoverageFieldPairs__c.getAll().values())
		{
			accountCoverageFieldByContactCoverageFieldMap.put(pair.Contact_Coverage_Field__c, pair.Account_Coverage_Field__c);
		}
		
		for(Ace_AC_Roll_Down_Employee_Filter__c pair : Ace_AC_Roll_Down_Employee_Filter__c.getAll().values())
		{
			Set<String> valueSet = employeeFilterMap.get(pair.Employee_Field__c);
			if(valueSet==null)
			{
				valueSet = new Set<String>();
				employeeFilterMap.put(pair.Employee_Field__c, valueSet);
			}
			valueSet.add(pair.Employee_Field_Value__c);
		}
		
		for(Ace_AC_Roll_Down_Contact_To_AC__c pair : Ace_AC_Roll_Down_Contact_To_AC__c.getAll().values())
		{
			contactToAccountCoverageFilterMap.put(pair.Contact_Multiselect_Field__c, pair.Account_Coverage_Field__c);
		}
		if(contactToAccountCoverageFilterMap.size()>0)
		{
			accountCoverageToContactField = contactToAccountCoverageFilterMap.values()[0];
			
			String[] contactFields = new String[]{};
			contactFields.addAll(contactToAccountCoverageFilterMap.keySet());
			contactToAccountCoverageField = contactFields[0];
		}
		if(accountCoverageFieldByContactCoverageFieldMap.size()>0 && contactToAccountCoverageFilterMap.size()>0)
		{
			isConfigured = true;
		}
	}


	///////////////////////////////////////////////////////////////////////////
	// Public methods
	///////////////////////////////////////////////////////////////////////////
	public void insertContactCoverageFromAccountCoverages(T1C_Base__Account_Coverage__c[] accountCoverages)
	{
		if(isConfigured==false)
		{
			return;
		}
		setAccountCoverages(accountCoverages);
		initEmployeeIdsFromAccountCoverage();
		this.accountCoverages = filterAccountCoverageByEmployees(this.accountCoverages, getEmployeeIds());
		initContactsFromAccountCoverages();
		if(this.contacts.size()==0)
		{
			return;
		}
		
		T1C_Base__Contact_Coverage__c[] ccsToInsert = new T1C_Base__Contact_Coverage__c[]{};
		
		for(T1C_Base__Account_Coverage__c ac : this.accountCoverages)
		{
			ccsToInsert.addAll(createContactCoverageFromAccountCoverageAndContacts(ac));
		}
		insertBatches(ccsToInsert);
		
	}
	
	/**
	 * Determine the type of update to the account coverage, to see if it will require new Contact Coverage records
	 * or an update in existing Contact Coverage records
	 */
	public void determineUpdateContactCoverageFromAccountCoverages(T1C_Base__Account_Coverage__c[] acsBeforeUpdate, T1C_Base__Account_Coverage__c[] acsAfterUpdate)
	{
		if(isConfigured==false)
		{
			return;
		}
		T1C_Base__Account_Coverage__c[] acsToDelete = new T1C_Base__Account_Coverage__c[]{};
		T1C_Base__Account_Coverage__c[] acsToInsert = new T1C_Base__Account_Coverage__c[]{};
		T1C_Base__Account_Coverage__c[] acsToUpdateOld = new T1C_Base__Account_Coverage__c[]{};
		T1C_Base__Account_Coverage__c[] acsToUpdateNew = new T1C_Base__Account_Coverage__c[]{};
				
		for(Integer i=0; i<acsBeforeUpdate.size(); i++)
		{
			if(requiresNewRecords(acsBeforeUpdate[i], acsAfterUpdate[i]))
			{
				acsToDelete.add(acsBeforeUpdate[i]);
				acsToInsert.add(acsAfterUpdate[i]);
			}
			else if(requiresCcUpdate(acsBeforeUpdate[i], acsAfterUpdate[i]))
			{
				acsToUpdateOld.add(acsBeforeUpdate[i]);
				acsToUpdateNew.add(acsAfterUpdate[i]);
			}
		}
		deleteContactCoverageFromAccountCoverages(acsToDelete);
		insertContactCoverageFromAccountCoverages(acsToInsert);
		updateContactCoverageFromAccountCoverages(acsToUpdateOld, acsToUpdateNew);
	}
	
	public void deleteContactCoverageFromAccountCoverages(T1C_Base__Account_Coverage__c[] accountCoverages)
	{
		if(isConfigured==false)
		{
			return;
		}
		setAccountCoverages(accountCoverages);
		initEmployeeIdsFromAccountCoverage();
		this.accountCoverages = filterAccountCoverageByEmployees(this.accountCoverages, getEmployeeIds());
		initContactsFromAccountCoverages();
		if(this.contacts.size()==0)
		{
			return;
		}
		
		T1C_Base__Contact_Coverage__c[] ccsToDelete = new T1C_Base__Contact_Coverage__c[]{};
		
		ccsToDelete.addAll(getMatchingContactCoverages(accountCoverages, this.contacts));

		deleteBatches(ccsToDelete);
	}
	
	public void insertContactCoverageFromContacts(Contact[] allContacts)
	{
		if(isConfigured==false)
		{
			return;
		}
		initAccountCoverages(allContacts);
		initEmployeeIdsFromAccountCoverage();
		this.accountCoverages = filterAccountCoverageByEmployees(this.accountCoverages, getEmployeeIds());
		initContacts(allContacts, this.accountCoverages);
		if(this.contacts.size()==0)
		{
			return;
		}
		
		T1C_Base__Contact_Coverage__c[] ccsToInsert = new T1C_Base__Contact_Coverage__c[]{};
		
		for(T1C_Base__Account_Coverage__c ac : this.accountCoverages)
		{
			ccsToInsert.addAll(createContactCoverageFromAccountCoverageAndContacts(ac));
		}
		insertBatches(ccsToInsert);
	}
	
	public void deleteContactCoverageFromContacts(Contact[] allContacts)
	{
		if(isConfigured==false)
		{
			return;
		}
		initAccountCoverages(allContacts);
		initEmployeeIdsFromAccountCoverage();
		this.accountCoverages = filterAccountCoverageByEmployees(this.accountCoverages, getEmployeeIds());
		initContacts(allContacts, this.accountCoverages);
		if(this.contacts.size()==0)
		{
			return;
		}
		
		T1C_Base__Contact_Coverage__c[] ccsToDelete = new T1C_Base__Contact_Coverage__c[]{};
		
		ccsToDelete.addAll(getMatchingContactCoverages(accountCoverages, this.contacts));

		deleteBatches(ccsToDelete);
	}
	
	/**
	 * Determine changes for contact decision maker for products, and add or remove contact coverage records
	 */
	// TODO: known limitation, changing contact's account may cause undetermined behaviour
	public void determineUpdateContactCoverageFromContact(Contact[] contactsBeforeUpdate, Contact[] contactsAfterUpdate)
	{
		if(isConfigured==false)
		{
			return;
		}
		Contact[] affectedContactsBeforeUpdate = new Contact[]{};
		Contact[] affectedContactsAfterUpdate = new Contact[]{};
		Contact[] contactsWithDeleteData = new Contact[]{};
		Contact[] contactsWithInsertData = new Contact[]{};
		
		for(Integer i=0; i<contactsBeforeUpdate.size(); i++)
		{
			// first filter out affected contacts
			if(isAffectedContact(contactsBeforeUpdate[i], contactsAfterUpdate[i])==true)
			{
				// then get products from the multiselect picklist, before and after the update
				affectedContactsBeforeUpdate.add(contactsBeforeUpdate[i]);
				affectedContactsAfterUpdate.add(contactsAfterUpdate[i]);

				Set<String> productSetBefore = new Set<String>();
				String productString =(String) contactsBeforeUpdate[i].get(contactToAccountCoverageField); 
				if(productString!=null)
				{
					productSetBefore.addAll(productString.split(';',0));
				}
				Set<String> productSetAfter = new Set<String>();
				productString = (String) contactsAfterUpdate[i].get(contactToAccountCoverageField);
				if(productString!=null)
				{
					productSetAfter.addAll(productString.split(';',0));
				}
		
				// get the products before the update, that are not there after the update.
				// these are products that need to be removed. 
				Contact c = getContactWithProductDelta(contactsBeforeUpdate[i], productSetBefore, productSetAfter);
				if(c!=null)
				{
					contactsWithDeleteData.add(c);
				}

				// get the products after the update, that were not there before the update.
				// these are products that need to be added.
				c = getContactWithProductDelta(contactsBeforeUpdate[i], productSetAfter, productSetBefore);
				if(c!=null)
				{
					contactsWithInsertData.add(c);
				}
			}
		}
		// remove contact coverages for contacts whose product list has been removed  
		if(contactsWithDeleteData.size()>0)
		{
			deleteContactCoverageFromContacts(contactsWithDeleteData);
		}
		
		// add contact coverages for contacts whose product list has been added
		if(contactsWithInsertData.size()>0)
		{
			insertContactCoverageFromContacts(contactsWithInsertData);
		}
	}
	
	/**
	 * If employee was eligible for AC-CC synchronization before the employee update, but is no longer eligible (or vice versa),
	 * that emmployees contact coverage records will be deleted (or added)
	 */
	public void determineUpdateContactCoverageFromEmployees(T1C_Base__Employee__c[] employeesBeforeUpdate, T1C_Base__Employee__c[] employeesAfterUpdate)
	{
		if(isConfigured==false)
		{
			return;
		}
		T1C_Base__Employee__c[] employeesToDelete = new T1C_Base__Employee__c[]{};
		T1C_Base__Employee__c[] employeesToInsert = new T1C_Base__Employee__c[]{};
				
		for(Integer i=0; i<employeesBeforeUpdate.size(); i++)
		{
			boolean isEmployeeBeforeUpdateEligible = isEmployeeEligible(employeesBeforeUpdate[i]);
			boolean isEmployeeAfterUpdateEligible = isEmployeeEligible(employeesAfterUpdate[i]);
			
			if(isEmployeeBeforeUpdateEligible && !isEmployeeAfterUpdateEligible)
			{
				employeesToDelete.add(employeesBeforeUpdate[i]);
			}
			else if(!isEmployeeBeforeUpdateEligible && isEmployeeAfterUpdateEligible)
			{
				employeesToInsert.add(employeesAfterUpdate[i]);
			}
			// if(isEmployeeBeforeUpdateEligible && isEmployeeAfterUpdateEligible)
			// do nothing
			// if !isEmployeeBeforeUpdateEligible && !isEmployeeAfterUpdateEligible
			// do nothing
		}
		if(employeesToDelete.size()>0)
		{
			deleteContactCoverageFromEmployees(employeesToDelete);
		}
		if(employeesToInsert.size()>0)
		{
			insertContactCoverageFromEmployees(employeesToInsert);
		}
	}
	
	public void deleteContactCoverageFromEmployees(T1C_Base__Employee__c[] employees)
	{
		if(isConfigured==false)
		{
			return;
		}
		initEmployeeIdsFromEmployees(employees);
		this.accountCoverages = getAccountCoverages(getAccountCoverageFieldSet(), 'WHERE T1C_Base__Employee__c in ('+ getSoqlIds(employees) +')');
		// setAccountCoverages(accountCoverages);
		this.accountCoverages = filterAccountCoverageByEmployees(this.accountCoverages, getEmployeeIds());
		initContactsFromAccountCoverages();
		if(this.contacts.size()==0)
		{
			return;
		}
		
		T1C_Base__Contact_Coverage__c[] ccsToDelete = new T1C_Base__Contact_Coverage__c[]{};
		
		ccsToDelete.addAll(getMatchingContactCoverages(accountCoverages, this.contacts));

		deleteBatches(ccsToDelete);
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// Private helper methods
	///////////////////////////////////////////////////////////////////////////
	private void setAccountCoverages(T1C_Base__Account_Coverage__c[] value)
	{
		accountCoverages = value;
	}
	
	private Set<Id> getEmployeeIds()
	{
		return employeeIds;
	}
	
	private void filterEmployeeIds(Set<Id> allEmployeeIds)
	{
		employeeIds = new Set<Id>();
		if(allEmployeeIds.size()>0)
		{
			String query = 'SELECT Id FROM T1C_Base__Employee__c WHERE Id IN ('+ getSoql(allEmployeeIds) +')'+ getSoqlWhereOrClause(employeeFilterMap);
			T1C_Base__Employee__c[] employees = Database.query(query);
			for(T1C_Base__Employee__c e : employees)
			{
				employeeIds.add(e.Id);
			}
		}
	}
	
	static Set<String> getAccountCoverageFieldSetFromMap()
	{
		Set<String> result = new Set<String>();
		result.addAll(accountCoverageFieldByContactCoverageFieldMap.values());
		return result;
	}
	
	private void initEmployeeIdsFromAccountCoverage()
	{
		Set<Id> allEmployeeIds = new Set<Id>();
		if(this.accountCoverages!=null)
		{
			for(T1C_Base__Account_Coverage__c ac : this.accountCoverages)
			{
				allEmployeeIds.add(ac.T1C_Base__Employee__c);
			}
		}
		filterEmployeeIds(allEmployeeIds);
	}
	
	private T1C_Base__Account_Coverage__c[] filterAccountCoverageByEmployees(T1C_Base__Account_Coverage__c[] accountCoverages, Set<Id> employeeIds)
	{
		T1C_Base__Account_Coverage__c[] result = new T1C_Base__Account_Coverage__c[]{};
		if(accountCoverages!=null)
		{
			for(T1C_Base__Account_Coverage__c ac : accountCoverages)
			{
				if(employeeIds.contains(ac.T1C_Base__Employee__c))
				{
					result.add(ac);
				}
			}
		}
		return result;
	}
	
	private void initContactsFromAccountCoverages()
	{
		Set<Id> accountIdSet = new Set<Id>();
		String decisionMakerForClause = '';
		if(this.accountCoverages.size()==0)
		{
			this.contacts = new Contact[]{};
			return;
		}
		
		for(T1C_Base__Account_Coverage__c ac : this.accountCoverages)
		{
			accountIdSet.add(ac.T1C_Base__Account__c);
			
			String product = (String) ac.get(accountCoverageToContactField);
			if(product!=null && product!='')
			{
				if(decisionMakerForClause!='')
				{
					decisionMakerForClause += ' OR';
				}
				decisionMakerForClause += ' '+ contactToAccountCoverageField +' INCLUDES (\'' + product + '\')';
			}
		}
		if(decisionMakerForClause=='')
		{
			this.contacts = new Contact[]{};
		}
		else
		{
			decisionMakerForClause = ' AND (' + decisionMakerForClause + ')';
		
			String query = 'SELECT Id, AccountId, '+ contactToAccountCoverageField +' FROM Contact WHERE T1C_Base__Inactive__c = false' + 
				' AND AccountId In ('+ getSoql(accountIdSet) + ')' + decisionMakerForClause;
			this.contacts = Database.query(query);
		}
	}

	// contacts
	// get eligible contacts, by matching DMF with account coverage.product
	// get eligible account coverage, by matching DMF with account coverage.product	
	// get eligible employees, from account coverage
	
	private T1C_Base__Contact_Coverage__c[] createContactCoverageFromAccountCoverageAndContacts(T1C_Base__Account_Coverage__c accountCoverage)
	{
		T1C_Base__Contact_Coverage__c[] ccsToInsert = new T1C_Base__Contact_Coverage__c[] {};
		
		for(Contact c : contacts)
		{
			if(c.AccountId==accountCoverage.T1C_Base__Account__c)
			{
				if(isContactEligible(c, accountCoverage))
				{
					T1C_Base__Contact_Coverage__c cc = new T1C_Base__Contact_Coverage__c();
					cc = updateContactFieldsCoverageFromAccountCoverageFields(cc, accountCoverage, c.Id, false);
					ccsToInsert.add(cc);
				}
			}
		}
		return ccsToInsert;
	}

	private T1C_Base__Contact_Coverage__c[] removeBatch(T1C_Base__Contact_Coverage__c[] ccs)
	{
		T1C_Base__Contact_Coverage__c[] result = new T1C_Base__Contact_Coverage__c[]{};
		for(integer i=0; i<MAX_BATCH_SIZE; i++)
		{
			result.add(ccs.remove(0));
		}
		return result;
	}
	
	private void insertBatches(T1C_Base__Contact_Coverage__c[] ccs)
	{
		T1C_Base__Contact_Coverage__c[] batch;
		try
		{
			while(ccs.size() > MAX_BATCH_SIZE)
			{
				batch = removeBatch(ccs);
				database.upsert( batch, false );
			}
			database.upsert( ccs, false );
		}
		catch(System.DmlException e)
		{
			
		}
		//system.assert(false, ccs);
	}
	
	private void upsertBatches(T1C_Base__Contact_Coverage__c[] ccs)
	{
		T1C_Base__Contact_Coverage__c[] batch;
		try
		{
			while(ccs.size() > MAX_BATCH_SIZE)
			{
				batch = removeBatch(ccs);
				database.upsert( batch, false );
			}
			database.upsert( ccs, false );
		}
		catch(System.DmlException e)
		{
			
		}
	}
	
	private void deleteBatches(T1C_Base__Contact_Coverage__c[] ccs)
	{
		T1C_Base__Contact_Coverage__c[] batch;
		try
		{
			while(ccs.size() > MAX_BATCH_SIZE)
			{
				batch = removeBatch(ccs);
				database.delete( batch, false );
			}
			database.delete( ccs, false );
		}
		catch(System.DmlException e)
		{
			
		}
	}

	private boolean ccMatchesAc(T1C_Base__Contact_Coverage__c cc, T1C_Base__Account_Coverage__c ac)
	{
		for(String ccField : accountCoverageFieldByContactCoverageFieldMap.keySet())
		{
			String acField = accountCoverageFieldByContactCoverageFieldMap.get(ccField);
			if(ccField==CONTACT_FIELD)
			{
				if(cc.getSObject(CONTACT_RELATIONSHIP).get(ACCOUNT_ID_FIELD)!= ac.get(ACCOUNT_FIELD))
				{
					return false;
				} 
			}
			else if(cc.get(ccField)!=ac.get(acField))
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Determine if the Account Coverage change results in new Contact Coverage records being required
	 * Covers the following cases:
	 * 1) Account is changed
	 * 2) Account Coverage-to-Contact field is changed
	 */
	private boolean requiresNewRecords(T1C_Base__Account_Coverage__c acBeforeUpdate, T1C_Base__Account_Coverage__c acAfterUpdate)
	{
		// if account has changed
		if(acBeforeUpdate.get(ACCOUNT_FIELD)!=acAfterUpdate.get(ACCOUNT_FIELD))
		{
			return true;
		}
		// if field that maps to contact (eg. product) field has changed
		if(acBeforeUpdate.get(accountCoverageToContactField)!=acAfterUpdate.get(accountCoverageToContactField))
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Determine if the Account Coverage change results in an update to the Contact Coverage records being required
	 * Any fields in the Account Coverage-Contact Coverage field mapping are covered
	 */
	private boolean requiresCcUpdate(T1C_Base__Account_Coverage__c acBeforeUpdate, T1C_Base__Account_Coverage__c acAfterUpdate)
	{
		for(String acField : accountCoverageFieldByContactCoverageFieldMap.values())
		{
			if(acBeforeUpdate.get(acField)!=acAfterUpdate.get(acField))
			{
				return true;
			}
		}
		return false;
	}
	
	private void updateContactCoverageFromAccountCoverages(T1C_Base__Account_Coverage__c[] acsBeforeUpdate, T1C_Base__Account_Coverage__c[] acsAfterUpdate)
	{
		setAccountCoverages(acsBeforeUpdate);
		initEmployeeIdsFromAccountCoverage();
		this.accountCoverages = filterAccountCoverageByEmployees(this.accountCoverages, getEmployeeIds());
		initContactsFromAccountCoverages();
		if(this.contacts.size()==0)
		{
			return;
		}
		
		T1C_Base__Contact_Coverage__c[] ccsToUpdate = new T1C_Base__Contact_Coverage__c[]{};
		
		Map<Id, T1C_Base__Contact_Coverage__c[]> contactCoveragesByAccountCoverageId = findContactCoverageMatchingAccountCoverages(acsBeforeUpdate, this.contacts);
		for(Integer i=0; i<acsBeforeUpdate.size(); i++)
		{
			T1C_Base__Account_Coverage__c ac = acsBeforeUpdate[i];
			T1C_Base__Contact_Coverage__c[] ccs = contactCoveragesByAccountCoverageId.get(ac.Id);
			if(ccs!=null)
			{
				for(T1C_Base__Contact_Coverage__c cc : ccs)
				{
					ccsToUpdate.add(updateContactFieldsCoverageFromAccountCoverageFields(cc, acsAfterUpdate[i], null, true));
				}
			}
		}
		upsertBatches(ccsToUpdate);
	}
	
	private T1C_Base__Contact_Coverage__c updateContactFieldsCoverageFromAccountCoverageFields(T1C_Base__Contact_Coverage__c cc, T1C_Base__Account_Coverage__c ac, Id contactId, boolean isUpdate)
	{
		for(String ccField : accountCoverageFieldByContactCoverageFieldMap.keySet())
		{
			String acField = accountCoverageFieldByContactCoverageFieldMap.get(ccField);
			if(ccField=='T1C_Base__Contact__c')
			{
				if(contactId!=null)
				{
					cc.put(ccField, contactId);
				}
			}
			else if(ccField.indexOf('Date')>=0)
			{
				cc.put(ccField, (Date) ac.get(acField));
			}
			else if(ccField=='T1C_Base__Is_Backup__c')
			{
				cc.put(ccField, (boolean) ac.get(acField));
			}
			else if(isUpdate==true && ccField=='T1C_Base__Employee__c')
			{
				// do nothing
			}
			else
			{
				cc.put(ccField, (String) ac.get(acField));
			}
		}
		return cc;
	}
	
	private T1C_Base__Contact_Coverage__c[] getMatchingContactCoverages(T1C_Base__Account_Coverage__c[] acs, Contact[] contacts)
	{
		Map<Id, T1C_Base__Contact_Coverage__c[]> contactCoverageListByAccountId = findContactCoverageMatchingAccountCoverages(acs, contacts);
		
		T1C_Base__Contact_Coverage__c[] matchingContactCoverages = new T1C_Base__Contact_Coverage__c[]{};
		for(T1C_Base__Contact_Coverage__c[] ccs : contactCoverageListByAccountId.values())
		{
			matchingContactCoverages.addAll(ccs);
		}
		return matchingContactCoverages;
	}
	
	private void initAccountCoverages(Contact[] allContacts)
	{
		Set<String> accountCoverageFieldSet = getAccountCoverageFieldSet();
		Map<Id, List<Contact>> contactsByAccountId = new Map<Id, List<Contact>>();
		Set<String> products = new Set<String>();
		
		for(Contact c : allContacts)
		{
			String productString = (String) c.get(contactToAccountCoverageField);
			if(productString!=null && c.AccountId != null)
			{
				products.addAll(productString.split(';',0));
				List<Contact> contacts = contactsByAccountId.get(c.AccountId);
				if(contacts==null)
				{
					contacts = new List<Contact>();
					contactsByAccountId.put(c.AccountId, contacts);
				}
				contacts.add(c);
			}
		}
		Set<Id> accountIds = contactsByAccountId.keySet();
		if(accountIds==null || accountIds.size()==0 || products ==null || products.size()==0)
		{
			this.accountCoverages = new T1C_Base__Account_Coverage__c[]{};
			return;
		}
		
		this.accountCoverages = getAccountCoverages(accountCoverageFieldSet, 'WHERE T1C_Base__Account__c in ('+ getSoql(contactsByAccountId.keySet()) +') AND ' +accountCoverageToContactField + ' IN (' + getSoql(products) +')');
		if(allContacts.size()>1)
		{
			T1C_Base__Account_Coverage__c[] eligibleAccountCoverages = new T1C_Base__Account_Coverage__c[]{};
			// because there is more than 1 contact, we need to match the contact with the associated account coverage
			for(T1C_Base__Account_Coverage__c ac : this.accountCoverages)
			{
				List<Contact> contacts = contactsByAccountId.get(ac.T1C_Base__Account__c);
				for(Contact c : contacts)
				{
					if(isContactEligible(c, ac))
					{
						eligibleAccountCoverages.add(ac);
						break;
					}
				}
			}
			this.accountCoverages = eligibleAccountCoverages;
		}
	}

	private void initContacts(Contact[] allContacts, T1C_Base__Account_Coverage__c[] accountCoverages)
	{
		// map account coverage by account
		Map<Id, Map<Id, T1C_Base__Account_Coverage__c>> accountCoverageByIdByAccountId = new Map<Id, Map<Id, T1C_Base__Account_Coverage__c>>();
		
		for(T1C_Base__Account_Coverage__c ac : accountCoverages) 
		{
			Map<Id, T1C_Base__Account_Coverage__c> accountCoverageById = accountCoverageByIdByAccountId.get(ac.T1C_Base__Account__c);
			if(accountCoverageById==null)
			{
				accountCoverageById = new Map<Id, T1C_Base__Account_Coverage__c>();
				accountCoverageByIdByAccountId.put(ac.T1C_Base__Account__c, accountCoverageById);
			}
			accountCoverageById.put(ac.Id, ac);
		}
		
		contacts = new Contact[]{};
		for(Contact c : allContacts)
		{
			Map<Id, T1C_Base__Account_Coverage__c> accountCoverageById = accountCoverageByIdByAccountId.get(c.AccountId);
			if(accountCoverageById!=null)
			{
				for(T1C_Base__Account_Coverage__c ac : accountCoverageById.values())
				{
					if(isContactEligible(c, ac))
					{
						contacts.add(c);
						break;
					}
				}
			}
		}
	}

	/**
	 * Determine products that are in the first set, but not in the second set, and return them back as a Contact object,
	 * which will be used when deleting or creating Contact Coverage
	 */
	private Contact getContactWithProductDelta(Contact contact, Set<String> productSetBefore, Set<String> productSetAfter)
	{
		Set<String> productDeltaSet = productSetBefore.clone();
		productDeltaSet.removeAll(productSetAfter);
		Contact c = contact.clone(true);
		String productDeltaString = '';
		for(String s : productDeltaSet)
		{
			if(productDeltaString!='')
			{
				productDeltaString += ';';
			}
			productDeltaString += s;
		}
		if(productDeltaString=='')
		{
			return null;
		}
		else
		{
			c.put(contactToAccountCoverageField, productDeltaString);
			return c;
		} 
	}
	
	/**
	 * Has the criteria field been changed?
	 */
	private boolean isAffectedContact(Contact oldContact, Contact newContact)
	{
		if(oldContact.get(contactToAccountCoverageField)==newContact.get(contactToAccountCoverageField))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	private boolean isEmployeeEligible(T1C_Base__Employee__c e)
	{
		for(String s : employeeFilterMap.keySet())
		{
			Set<String> valueSet = employeeFilterMap.get(s);
			if(valueSet.contains((String) e.get(s))==false)
			{
				return false;
			}
		}
		return true;
	}
	
	private void insertContactCoverageFromEmployees(T1C_Base__Employee__c[] employees)
	{
		initEmployeeIdsFromEmployees(employees);
		this.accountCoverages = getAccountCoverages(getAccountCoverageFieldSet(), 'WHERE T1C_Base__Employee__c in ('+ getSoqlIds(employees) +')');
		// setAccountCoverages(accountCoverages);
		this.accountCoverages = filterAccountCoverageByEmployees(this.accountCoverages, getEmployeeIds());
		initContactsFromAccountCoverages();
		if(this.contacts.size()==0)
		{
			return;
		}
		
		T1C_Base__Contact_Coverage__c[] ccsToInsert = new T1C_Base__Contact_Coverage__c[]{};
		
		for(T1C_Base__Account_Coverage__c ac : this.accountCoverages)
		{
			ccsToInsert.addAll(createContactCoverageFromAccountCoverageAndContacts(ac));
		}
		insertBatches(ccsToInsert);
	}
	
	private void initEmployeeIdsFromEmployees(T1C_Base__Employee__c[] employees)
	{
		employeeIds = new Set<Id>();
		for(T1C_Base__Employee__c e : employees)
		{
			if(isEmployeeEligible(e)==true)
			{
				employeeIds.add(e.Id);
			}
		}
	}
	
	static private Set<String> getAccountCoverageFieldSet()
	{
		Set<String> accountCoverageFieldSet = getAccountCoverageFieldSetFromMap();
		accountCoverageFieldSet.add('Id');
		accountCoverageFieldSet.add('T1C_Base__Account__c');
		accountCoverageFieldSet.add('T1C_Base__Employee__c');
		accountCoverageFieldSet.add('T1C_Base__Product__c');
		
		return accountCoverageFieldSet;
	}
	
	static private String getSoqlIds(SObject[] objs)
	{
		String result = '';
		for(SObject s: objs)
		{
			if(result!='')
			{
				result += ',';
			}
			result += '\'' + s.Id + '\'';
		}
		return result;
	}
	
	static private String getSoql(Set<String> stringSet)
	{
		String result = '';
		for(String s: stringSet)
		{
			if(result!='')
			{
				result += ',';
			}
			result += '\'' + s + '\'';
		}
		return result;
	}
	
	static private String getSoqlFields(Set<String> fieldSet)
	{
		String result = '';
		for(String s : fieldSet)
		{
			if(result!='')
			{
				result += ',';
			}
			result += s;
		}
		return result;
	}
	
	static private String getSoql(Set<Id> ids)
	{
		String result = '';
		for(Id i: ids)
		{
			if(result!='')
			{
				result += ',';
			}
			result += '\'' + i + '\'';
		}
		return result;
	}
	
	static private T1C_Base__Account_Coverage__c[] getAccountCoverages(Set<String> accountCoverageFieldSet, String whereClause)
	{
		String query = 'SELECT '+ getSoqlFields(accountCoverageFieldSet) +' FROM T1C_Base__Account_Coverage__c ' + whereClause;
		return Database.query(query);
	}
	
	static private String getSoqlWhereOrClause(Map<String, Set<String>> whereMap)
	{
		String result = '';
		for(String s : whereMap.keySet())
		{
			result += ' AND ' + s + ' IN ('+ getSoql(whereMap.get(s)) +')';
		}
		return result;
	}
	
	// remove contact coverage if it's valid contact and valid employee
	// filtering by the account coverage
	private Map<Id, T1C_Base__Contact_Coverage__c[]> findContactCoverageMatchingAccountCoverages(T1C_Base__Account_Coverage__c[] accountCoverages, Contact[] contacts)
	{
		Map<Id, T1C_Base__Contact_Coverage__c[]> result = new Map<Id, T1C_Base__Contact_Coverage__c[]>();
		
		Set<Id> contactIds = new Set<Id>();
		for(Contact c : contacts)
		{
			contactIds.add(c.Id);
		}
		
		if(contactIds.size()>0)
		{
			String query = 'SELECT '+ getSoqlFields(getContactCoverageFieldSet()) +' FROM T1C_Base__Contact_Coverage__c WHERE T1C_Base__Contact__c IN ('+ getSoql(contactIds) +') AND T1C_Base__Employee__c IN ('+ getSoql(this.employeeIds) +')';
			for	(T1C_Base__Contact_Coverage__c[] ccs : Database.query(query))
			{
				for(T1C_Base__Contact_Coverage__c cc : ccs)
				{
					for(T1C_Base__Account_Coverage__c ac : accountCoverages)
					{
						if(ccMatchesAc(cc, ac))
						{
							T1C_Base__Contact_Coverage__c[] ccsForAcId = result.get(ac.Id);
							if(ccsForAcId==null)
							{
								ccsForAcId = new T1C_Base__Contact_Coverage__c[]{};
								result.put(ac.Id, ccsForAcId);
							}
							ccsForAcId.add(cc);
						}
					}
				}
			}
		}
		return result;
	}
	
	static private boolean isContactEligible(Contact c, T1C_Base__Account_Coverage__c ac)
	{
		for(String contactField : contactToAccountCoverageFilterMap.keySet())
		{
			String acField = contactToAccountCoverageFilterMap.get(contactField);
			String fieldValue = (String) c.get(contactField);
			if(fieldValue==null)
			{
				// if decision maker for nothing, then contact is not eligible
				return false;
			}
			String[] values = fieldValue.split(';',0);
			Set<String> valueSet = new Set<String>();
			valueSet.addAll(values);
			
			if(valueSet.contains((String) ac.get(acField))==false)
			{
				return false;
			}
		}
		return true;
	}
	
	static private Set<String> getContactCoverageFieldSet()
	{
		Set<String> contactCoverageFieldSet = new Set<String>();
		contactCoverageFieldSet.addAll(accountCoverageFieldByContactCoverageFieldMap.keySet());
		contactCoverageFieldSet.add('Id');
		contactCoverageFieldSet.add('T1C_Base__Contact__r.AccountId');
		
		return contactCoverageFieldSet;
	}
	
}