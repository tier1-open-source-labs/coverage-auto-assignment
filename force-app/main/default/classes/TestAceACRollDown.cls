/*
 * Name:    TestAceACRollDown
 * Version: 2.2
 * Description: 
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */
@isTest (seeAllData = false)
private class TestAceACRollDown 
{
	private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

	private static Account acct;
	private static Contact cont;
	private static T1C_Base__Employee__c emp;
	private static T1C_Base__Account_Coverage__c ac;
	
	private static void setupData()
	{
		dataFactory.createCBSAFR();
		
		Ace_AC_Roll_Down_Contact_To_AC__c contactToAC = new Ace_AC_Roll_Down_Contact_To_AC__c(Name = 'Map1', Contact_Multiselect_Field__c = 'T1C_Base__Job_Function__c', Account_Coverage_Field__c = 'T1C_Base__Role__c');
		insert contactToAC;
		
		Ace_AC_Roll_Down_Employee_Filter__c filter = new Ace_AC_Roll_Down_Employee_Filter__c(Name = 'Map1', Employee_Field_Value__c = 'CM', Employee_Field__c = 'T1C_Base__Department__c');
		insert filter;
		
		insert new list<ContactCoverageAccountCoverageFieldPairs__c>
		{
			new ContactCoverageAccountCoverageFieldPairs__c(Name = 'Map1', Account_Coverage_Field__c = 'T1C_Base__Employee__c', Contact_Coverage_Field__c = 'T1C_Base__Employee__c'),
			new ContactCoverageAccountCoverageFieldPairs__c(Name = 'Map2', Account_Coverage_Field__c = 'T1C_Base__Account__c', Contact_Coverage_Field__c = 'T1C_Base__Contact__c'),
			new ContactCoverageAccountCoverageFieldPairs__c(Name = 'Map3', Account_Coverage_Field__c = 'T1C_Base__Role__c', Contact_Coverage_Field__c = 'T1C_Base__Role__c'),
			new ContactCoverageAccountCoverageFieldPairs__c(Name = 'Map4', Account_Coverage_Field__c = 'T1C_Base__Is_Backup__c', Contact_Coverage_Field__c = 'T1C_Base__Is_Backup__c'),
			new ContactCoverageAccountCoverageFieldPairs__c(Name = 'Map5', Account_Coverage_Field__c = 'T1C_Base__Start_Date__c', Contact_Coverage_Field__c = 'T1C_Base__Start_Date__c'),
			new ContactCoverageAccountCoverageFieldPairs__c(Name = 'Map6', Account_Coverage_Field__c = 'T1C_Base__End_Date__c', Contact_Coverage_Field__c = 'T1C_Base__End_Date__c')
		};
		
		emp = dataFactory.makeLoggedInUserEmployee();
		emp.T1C_Base__Department__c = 'CM';
		insert emp;
		
		acct = dataFactory.makeAccount('Test Account');
		insert acct;
		
		cont = dataFactory.makeContact(acct.Id, 'Marla', 'Singer');
    	cont.T1C_Base__Job_Function__c = 'Trader';
        insert cont;
        
        ac = dataFactory.makeAccountCoverage(acct.Id, emp.Id);
        insert ac;
	}
	
    
    static testMethod void testTrigger()
    {
        setupData();
        
        test.startTest();

        T1C_Base__Contact_Coverage__c[] ccs = [SELECT Id FROM T1C_Base__Contact_Coverage__c WHERE T1C_Base__Employee__c = :emp.Id AND T1C_Base__Contact__c = :cont.Id];
        System.assertEquals(ccs.size(), 0);
        
        ac.T1C_Base__Role__c = 'Trader';
        update ac;
        
        System.assertEquals(ac.T1C_Base__Role__c,cont.T1C_Base__Job_Function__c);
        
        ccs = [SELECT Id FROM T1C_Base__Contact_Coverage__c WHERE T1C_Base__Employee__c = :emp.Id AND T1C_Base__Contact__c = :cont.Id];
        
        ac.T1C_Base__Is_Backup__c = true;
        update ac;

        ccs = [SELECT Id FROM T1C_Base__Contact_Coverage__c WHERE T1C_Base__Employee__c = :emp.Id AND T1C_Base__Contact__c = :cont.Id];
        
        delete ac;
        
        test.stopTest();
    }
    static testMethod void testContTrigger()
    {
        setupData();
               
        test.startTest();
        
        T1C_Base__Contact_Coverage__c[] ccs = [SELECT Id FROM T1C_Base__Contact_Coverage__c WHERE T1C_Base__Employee__c = :emp.Id AND T1C_Base__Contact__c = :cont.Id];
        
        cont.T1C_Base__Job_Function__c = '';
        update cont;

        ccs = [SELECT Id FROM T1C_Base__Contact_Coverage__c WHERE T1C_Base__Employee__c = :emp.Id AND T1C_Base__Contact__c = :cont.Id];
        System.assertEquals(ccs.size(), 0);
        
        cont.T1C_Base__Job_Function__c = 'Trader';
        update cont;
        
        ccs = [SELECT Id FROM T1C_Base__Contact_Coverage__c WHERE T1C_Base__Employee__c = :emp.Id AND T1C_Base__Contact__c = :cont.Id];
        
        delete cont;
        
        test.stopTest();
    }
    static testMethod void testEmpTrigger()
    {
        setupData();
               
        test.startTest();
                
        T1C_Base__Contact_Coverage__c[] ccs = [SELECT Id FROM T1C_Base__Contact_Coverage__c WHERE T1C_Base__Employee__c = :emp.Id AND T1C_Base__Contact__c = :cont.Id];
        emp.T1C_Base__Department__c = '';
        update emp;

        ccs = [SELECT Id FROM T1C_Base__Contact_Coverage__c WHERE T1C_Base__Employee__c = :emp.Id AND T1C_Base__Contact__c = :cont.Id];
        System.assertEquals(ccs.size(), 0);
        
        emp.T1C_Base__Department__c = 'CM';
        update emp;

        ccs = [SELECT Id FROM T1C_Base__Contact_Coverage__c WHERE T1C_Base__Employee__c = :emp.Id AND T1C_Base__Contact__c = :cont.Id];
        
        delete ac;
        delete emp;

        test.stopTest();
    }
}