/** 
 * Name: testContCoverageRollup 
 * Version: 2.2
 * Description: Trigger to synchronize the AccountTeamMember and ContactShare
 *              records with inserts/updates/deletes to Contact_Share__c records.
 *
 * Confidential & Proprietary, 2013 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
@isTest (SeeAllData=false)
private class testContCoverageRollup 
{   
	private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

    static Account acct0 , acct1;
    static Contact cont00 , cont01 , cont02 , cont10 , cont11 , cont12;
    static User user0, user1;
    
    static T1C_Base__Employee__c empl0 = dataFactory.makeEmployee('James', 'Durango', null);
    static T1C_Base__Employee__c empl1 = dataFactory.makeEmployee('Stephen', 'Thomson', null);

    
    static final String TEST_ACCT0_NAME = 'Cyberdyne Systems'; 
    static final String TEST_ACCT1_NAME = 'Paper Street Soap Company'; 
    
    static final String TEST_CONT00_LNAME = 'Dyson'; 
    static final String TEST_CONT01_LNAME = 'Connor'; 
    static final String TEST_CONT02_LNAME = 'Singer'; 
    static final String TEST_CONT10_LNAME = 'Durden'; 
    static final String TEST_CONT11_LNAME = 'Smith'; 
    static final String TEST_CONT12_LNAME = 'Stuart'; 
    
    static User thisUser;

    static final String BACKUP = 'Back';
    static final String PRIMARY = 'Primary';
    static final String OTHERS = 'Others';

    static final Date TEST_START_DATE = Date.Today ().addDays ( -30 );
    static final Date TEST_END_DATE = Date.Today ().addDays ( 30 );

    static void setupData() 
    {
        dataFactory.createCBSAFR();
        
        acct0 = dataFactory.makeAccount(TEST_ACCT0_NAME);
        acct1 = dataFactory.makeAccount(TEST_ACCT1_NAME);
        
        Insert new Account[] { acct0 , acct1 };     
        
        cont00 = dataFactory.makeContact(acct0.Id,'F'+TEST_CONT00_LNAME, TEST_CONT00_LNAME);
        cont01 = dataFactory.makeContact(acct0.Id,'F'+TEST_CONT01_LNAME, TEST_CONT01_LNAME);
        cont02 = dataFactory.makeContact(acct0.Id,'F'+TEST_CONT02_LNAME, TEST_CONT02_LNAME);
        cont10 = dataFactory.makeContact(acct1.Id,'F'+TEST_CONT10_LNAME, TEST_CONT10_LNAME);
        cont11 = dataFactory.makeContact(acct1.Id,'F'+TEST_CONT11_LNAME, TEST_CONT11_LNAME);
        cont12 = dataFactory.makeContact(acct1.Id,'F'+TEST_CONT12_LNAME, TEST_CONT12_LNAME);
        
        insert new Contact[] { cont00 , cont01 , cont02 , cont10 , cont11 , cont12 };
        
        insert new list<T1C_Base__Employee__c>{empl0,empl1};
    }

    static Map<Id , Map<Id , T1C_Base__Account_Coverage__c>> getAcctCoverageMap () 
    {
        Map<Id, Map<Id, T1C_Base__Account_Coverage__c>> acMap = new Map<Id, Map<Id, T1C_Base__Account_Coverage__c>>{};

        for ( T1C_Base__Account_Coverage__c ac : [ select T1C_Base__Employee__c , T1C_Base__Account__c , T1C_Base__Start_Date__c , T1C_Base__End_Date__c , T1C_Base__Role__c
                                            from T1C_Base__Account_Coverage__c
                                            where T1C_Base__Account__c in ( :acct0.Id , :acct1.Id ) ])
        {
            if ( !acMap.containsKey ( ac.T1C_Base__Employee__c ) ) 
            {
                acMap.put ( ac.T1C_Base__Employee__c , new Map<Id , T1C_Base__Account_Coverage__c> {} );
            }
            acMap.get ( ac.T1C_Base__Employee__c ).put ( ac.T1C_Base__Account__c , ac );
        }
        
        return acMap;
    }
    
    static void clearCCFilters () 
    {
//      Grab the filters from the Custom Setting.
//      -----------------------------------------
        SObject[] ccrfList;
        String soqlQry = 'select Id from Cont_Cov_Rollup_Filter__c';
        
        try 
        {
            ccrfList = Database.query ( soqlQry );
        }
        catch ( QueryException e ) 
        {
//          Custom setting not defined.
//          ---------------------------
            return;
        }
        
        Database.delete ( ccrfList );
    }
    
    static Boolean setCCFilter ( String name , String value ) 
    {
        Cont_Cov_Rollup_Filter__c filter = new Cont_Cov_Rollup_Filter__c(Name = 'Default', FieldName__c = name, Exclusion_Filter__c = value);
        insert filter;
        
        return True;
    }

    public static TestMethod void testCoverageFilters () 
    {
        setupData();
        
        Test.startTest (); 
    
//      Set a really bogus Coverage filter.
//      -----------------------------------
        setCCFilter('T1C_Base__Role__c' , '= \'' + PRIMARY + '\'' ); 
                    
//      Create a Test coverage.  Rollup should be suppressed by the Coverage filter.
//      ----------------------------------------------------------------------------
        T1C_Base__Contact_Coverage__c cc1 = dataFactory.makeContactCoverage(cont00.Id,empl0.Id);
        cc1.T1C_Base__Role__c = PRIMARY;
        insert cc1;
        
        Map<Id , Map<Id , T1C_Base__Account_Coverage__c>> acMap = getAcctCoverageMap ();
        System.assertEquals ( Null , acMap.get ( empl0.Id ) );
        
//      Remove the Coverage filter.
//      ---------------------------
        clearCCFilters ();

//      Re-insert the Test coverage.  Rollup should now happen.
//      -------------------------------------------------------
        delete cc1;
        
        cc1 = cc1.clone ( False , True );
        
        insert cc1;

        Test.stopTest();
    }
    
    public static TestMethod void testStartEndDates () 
    {
       	setupData();
        
        Test.startTest ();      
            
//      Create a bunch of Test coverages.
//      ---------------------------------
        
        T1C_Base__Contact_Coverage__c cc1 = dataFactory.makeContactCoverage(cont00.Id,empl0.Id);
        T1C_Base__Contact_Coverage__c cc2 = dataFactory.makeContactCoverage(cont01.Id,empl0.Id);
        T1C_Base__Contact_Coverage__c cc3 = dataFactory.makeContactCoverage(cont10.Id,empl0.Id);
        T1C_Base__Contact_Coverage__c cc4 = dataFactory.makeContactCoverage(cont11.Id,empl0.Id);       

        cc1.T1C_Base__Start_Date__c = TEST_START_DATE;
        cc2.T1C_Base__Start_Date__c = TEST_START_DATE.addDays ( -1 );
        cc3.T1C_Base__Start_Date__c = TEST_START_DATE;
        cc4.T1C_Base__Start_Date__c = TEST_START_DATE.addDays ( -1 );
   
        cc1.T1C_Base__End_Date__c = TEST_END_DATE;
        cc2.T1C_Base__End_Date__c = TEST_END_DATE.addDays ( -1 );
        cc3.T1C_Base__End_Date__c = TEST_END_DATE;
        cc4.T1C_Base__End_Date__c = TEST_END_DATE.addDays ( -1 );
 
        cc1.T1C_Base__Role__c = PRIMARY;
        cc2.T1C_Base__Role__c = BACKUP;
        cc3.T1C_Base__Role__c = OTHERS;
        cc4.T1C_Base__Role__c = BACKUP;
      
        insert new list<T1C_Base__Contact_Coverage__c>{cc1,cc2,cc3,cc4};
                
        Map<Id , Map<Id , T1C_Base__Account_Coverage__c>> acMap = getAcctCoverageMap();
        T1C_Base__Account_Coverage__c ac;
    
        ac = acMap.get(empl0.Id).get(acct0.Id);
        System.assertNotEquals ( Null , ac );
        System.assertEquals ( PRIMARY , ac.T1C_Base__Role__c );
        
        ac = acMap.get(empl0.Id).get(acct1.Id);
        System.assertNotEquals ( Null , ac );

//      Update a Contact Coverage, changing its End Date and Role.
//      ----------------------------------------------------------
        cc4.T1C_Base__End_Date__c = TEST_END_DATE.addDays ( 1 );
        cc4.T1C_Base__Role__c = OTHERS;
        update cc4;
        
        acMap = getAcctCoverageMap();
        
        System.assertNotEquals(Null ,acMap.get(empl0.Id));

        ac = acMap.get(empl0.Id).get(acct1.Id);
        
//      Update another Contact Coverage, moving it to a Contact on another Account.
//      ---------------------------------------------------------------------------    
        cc2.T1C_Base__Contact__c = cont12.Id;
        update cc2;
        delete cc2;
        
        Test.stopTest();
    }
        
    
    public static TestMethod void testIgnoreTempCoverage () 
    {           
        setupData();
        
        Test.startTest ();    
        
//      Create a Temporary Test coverage.  Rollup should be suppressed by the Coverage filter.
//      --------------------------------------------------------------------------------------
        T1C_Base__Employee_Away_Date__c ead = dataFactory.makeEmpAwayDate(empl1.Id, Date.Today(), 1);
        insert ead;
            
     	T1C_Base__Contact_Coverage__c cc1 = dataFactory.makeContactCoverage(cont00.Id,empl0.Id);
        T1C_Base__Contact_Coverage__c cc2 = dataFactory.makeContactCoverage(cont01.Id,empl0.Id);
        T1C_Base__Contact_Coverage__c cc3 = dataFactory.makeContactCoverage(cont10.Id,empl0.Id);
        
        cc1.T1C_Base__Start_Date__c = TEST_START_DATE;
        cc2.T1C_Base__Start_Date__c = TEST_START_DATE;
        cc3.T1C_Base__Start_Date__c = TEST_START_DATE;
        
        cc1.T1C_Base__End_Date__c = TEST_END_DATE;
        cc2.T1C_Base__End_Date__c = TEST_END_DATE;
        cc3.T1C_Base__End_Date__c = TEST_END_DATE;
        
        cc1.T1C_Base__Role__c = PRIMARY;
        cc2.T1C_Base__Role__c = BACKUP;
        cc3.T1C_Base__Role__c = 'test test';
        
        cc1.T1C_Base__Employee_Away_Date__c = ead.Id;
        cc2.T1C_Base__Employee_Away_Date__c = ead.Id;
        cc3.T1C_Base__Employee_Away_Date__c = ead.Id;
           
        insert new list<T1C_Base__Contact_Coverage__c>{cc1,cc2,cc3};
                
        cc3.T1C_Base__Role__c = null;
        update cc3;

        Test.stopTest();
    }    
}